package main // import "github.com/joyde68/blog"

import (
	"fmt"
	"github.com/joyde68/blog/app"
	"github.com/pkg/browser"
	"time"
)

func main() {
	go func() {
		time.Sleep(time.Second * 2)
		err := browser.OpenURL("http://127.0.0.1:4000")
		if err != nil {
			fmt.Println(err)
		}
	}()
	// Start server
	app.Init().Run()
}
