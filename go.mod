module github.com/joyde68/blog

go 1.12

require (
	github.com/Unknwon/cae v0.0.0-20160715032808-c6aac99ea2ca
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755 // indirect
	github.com/go-macaron/inject v0.0.0-20160627170012-d8a0b8677191 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190618222545-ea8f1a30c443 // indirect
	gopkg.in/ini.v1 v1.42.0 // indirect
	gopkg.in/macaron.v1 v1.3.2
)
