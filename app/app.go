package app

import (
	"fmt"
	"github.com/joyde68/blog/app/models"
	"github.com/joyde68/blog/app/utils"
	"github.com/joyde68/blog/app/handler"
	"gopkg.in/macaron.v1"
	"log"
	"os"
)

func Init() *macaron.Macaron {
	// Switch macaron to production mode
	macaron.Env = "production"
	// init application
	app := macaron.Classic()
	// set static handler
	app.Use(macaron.Static("public", macaron.StaticOptions{
		Prefix: "public",
	}))
	// init storage
	models.Init()
	// set not found handler
	app.NotFound(func(context *macaron.Context) {
		models.Theme(false).Tpl("404").Render(context, 404, nil)
	})
	// set recover handler
	app.InternalServerError(func(context *macaron.Context) {
		info := fmt.Sprintf("%s -> %s%s -> %d", context.Req.Method, context.Req.RemoteAddr, context.Req.RequestURI, 500)
		models.AddLog([]byte(info))
		models.Theme(false).Tpl("500").Render(context, 500, nil)
	})
	// Load local data
	models.All()
	// Init handler
	handler.InitRoutes(app)
	// Catch exit command
	go utils.CatchExit(saveAndCleanedFile)
	// return result
	return app
}

func saveAndCleanedFile() {
	log.Println("Ready to exit")
	models.SyncAll()
	log.Println("Save data successfully")
	if err := os.RemoveAll("tmp"); err != nil {
		log.Printf("Failed to clean up temporary files\n%s", err.Error())
		os.Exit(1)
	}
	log.Println("Successfully cleaned up temporary files")
	os.Exit(0)
}
