package models

import (
	"github.com/jmoiron/sqlx"
	"github.com/joyde68/blog/app/utils"
	_ "github.com/mattn/go-sqlite3"
	"log"
)

const (
	createUserTableSchema = `CREATE TABLE if not exists "users" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"name"	TEXT NOT NULL,
	"password"	TEXT NOT NULL,
	"nick"	TEXT NOT NULL,
	"email"	TEXT NOT NULL,
	"avatar"	TEXT NOT NULL,
	"url"	TEXT NOT NULL,
	"bio"	TEXT NOT NULL,
	"crate_time"	INTEGER NOT NULL,
	"last_login_time"	INTEGER NOT NULL,
	"role"	TEXT NOT NULL
);`
)

var (
	db *sqlx.DB
)

func Init() {
	StorageInit()
	LogInit()

	// open and connect at the same time, panicing on error
	db = sqlx.MustConnect("sqlite3", "data/data.db")
	// execute a query on the server
	_, err := db.Exec(createUserTableSchema)
	if err != nil {
		panic(err)
	}

	go utils.CatchExit(func() {
		log.Println("Close db")
		db.Close()
	})
}
