package models

import (
	"errors"
	"github.com/joyde68/blog/app/utils"
	"log"
)

var (
	users     []*User
	userMaxId int
)

type User struct {
	Id            uint   `db:"id"`
	Name          string `db:"name"`
	Password      string `db:"password"`
	Nick          string `db:"nick"`
	Email         string `db:"email"`
	Avatar        string `db:"avatar"`
	Url           string `db:"url"`
	Bio           string `db:"bio"`
	CreateTime    int64  `db:"crate_time"`
	LastLoginTime int64  `db:"last_login_time"`
	Role          string `db:"role"`
}

// check user password.
func (u *User) CheckPassword(pwd string) bool {
	return utils.Sha1(pwd+"xxxxx") == u.Password
}

// change user email.
// check unique.
func (u *User) ChangeEmail(email string) bool {
	u2 := GetUserByEmail(u.Email)
	if u2.Id != u.Id {
		return false
	}
	u.Email = email
	return true
}

// change user password.
func (u *User) ChangePassword(pwd string) {
	u.Password = utils.Sha1(pwd + "xxxxx")
}

// get a user by given id.
func GetUserById(id uint) *User {
	user := User{}
	log.Println(db)
	err := db.Get(&user, "SELECT * FROM users WHERE id = ?", id)
	if err != nil {
		log.Println(err)
		return nil
	}
	return &user
}

// get a user by given name.
func GetUserByName(name string) *User {
	for _, u := range users {
		if u.Name == name {
			return u
		}
	}
	return nil
}

// get a user by given email.
func GetUserByEmail(email string) *User {
	for _, u := range users {
		if u.Email == email {
			return u
		}
	}
	return nil
}

// get users of given role.
func GetUsersByRole(role string) []*User {
	us := make([]*User, 0)
	for _, u := range users {
		if u.Role == role {
			us = append(us, u)
		}
	}
	return us
}

// create new user.
func CreateUser(u *User) error {
	if GetUserByName(u.Email) != nil {
		return errors.New("email-repeat")
	}
	userMaxId += Storage.TimeInc(5)
	u.Id = uint(userMaxId)
	u.CreateTime = utils.Now()
	u.LastLoginTime = u.CreateTime
	users = append(users, u)
	go SyncUsers()
	return nil
}

// remove a user.
func RemoveUser(u *User) {
	for i, u2 := range users {
		if u2.Id == u.Id {
			users = append(users[:i], users[i+1:]...)
			break
		}
	}
	go SyncUsers()
}

// write users to json.
func SyncUsers() {
	Storage.Set("users", users)
}

func LoadUsers() {
	users = make([]*User, 0)
	userMaxId = 0
	Storage.Get("users", &users)
	for _, u := range users {
		if int(u.Id) > userMaxId {
			userMaxId = int(u.Id)
		}
	}
}
