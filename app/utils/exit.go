package utils

import (
	"os"
	"os/signal"
	"syscall"
)

// code from https://github.com/Unknwon/gowalker/blob/master/gowalker.go
func CatchExit(callback func()) {
	sigTerm := syscall.Signal(15)
	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, sigTerm)

	for {
		switch <-sig {
		case os.Interrupt, sigTerm:
			callback()
		}
	}
}