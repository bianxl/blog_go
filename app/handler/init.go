package handler

import "gopkg.in/macaron.v1"

func InitRoutes(app *macaron.Macaron) {
	// Register the foreground route
	registerHomeRoutes(app)
	// Registration management background routing
	registerAdminRoutes(app)
}

func registerHomeRoutes(app *macaron.Macaron) {
	app.Route("/login/", "GET,POST", login)
	app.Get("/logout/", logout)

	app.Get("/article/:slug", article)
	app.Get("/page/:slug/", page)
	app.Post("/comment/:id/", comment)
	app.Get("/tag/:tag/", tagArticles)
	app.Get("/tag/:tag/p/:page/", tagArticles)
	app.Get("/:slug", topPage)
	app.Get("/", home)
	app.Get("/p/:page/", home)

	app.Get("/feed/", rss)
	app.Get("/sitemap", siteMap)
}

func registerAdminRoutes(app *macaron.Macaron) {
	// add admin handlers
	app.Group("/admin", func() {
		app.Get("/", admin)
		app.Route("/profile/", "GET,POST", adminProfile)
		app.Route("/password/", "GET,POST", adminPassword)

		app.Get("/articles/", adminArticle)
		app.Get("/articles/p/:page/", adminArticle)
		app.Route("/article/write/", "GET,POST", articleWrite)
		app.Route("/article/:id/", "GET,POST,DELETE", articleEdit)

		app.Get("/pages/", adminPage)
		app.Get("/pages/p/:page/", adminPage)
		app.Route("/page/write/", "GET,POST", pageWrite)
		app.Route("/page/:id/", "GET,POST,DELETE", pageEdit)

		app.Route("/comments/", "GET,POST,PUT,DELETE", adminComments)

		app.Route("/settings/", "GET,POST", adminSetting)
		app.Post("/settings/custom/", customSetting)
		app.Post("/settings/nav/", navigatorSetting)

		app.Route("/message/", "GET,POST,DELETE", adminMessage)
		//app.Post("/message/read/", Auth, AdminMessageRead)
		app.Post("/message/read/", adminMessageRead)

		app.Get("/monitor/", adminMonitor)
		app.Route("/reader/", "GET,POST", adminReader)
		app.Route("/templates/", "GET,POST", adminTemplates)
		app.Route("/logs/", "GET,DELETE", adminLogs)
	}, auth)
}

